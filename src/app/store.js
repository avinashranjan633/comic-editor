import { configureStore } from '@reduxjs/toolkit';
import Filter from '../components/filter/Filter';

export default configureStore({
  reducer: {
    AppFilter: Filter,
  },
});