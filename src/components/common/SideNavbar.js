import React, { Component } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import Toolbar from '@mui/material/Toolbar';
import InterestsTwoToneIcon from '@mui/icons-material/InterestsTwoTone';
import InsertPhotoTwoToneIcon from '@mui/icons-material/InsertPhotoTwoTone';
import TextFieldsIcon from '@mui/icons-material/TextFields';
import { ListItemText, ListItemIcon, ListSubheader } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import Grid from '@mui/material/Grid';
import { Grade } from '@mui/icons-material';
import AllImages from '../shapes/AllImages';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import PhoneIcon from '@mui/icons-material/Phone';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PersonPinIcon from '@mui/icons-material/PersonPin';
import AllShapes from '../shapes/AllShapes';
import AllText from '../shapes/AllText';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      // style="width: -webkit-fill-available"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 1 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};


const SideNavbar = (props) => {


  const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.5),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.80),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(2),
      width: '90%',
    },
  }));

  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '12ch',
        '&:focus': {
          width: '20ch',
        },
      },
    },
  }));
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <div className='sidebar-content'>
      <div className="tabs">
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" style={{ background: '#18191b' }}>
          <Tab icon={<InterestsTwoToneIcon />} label="Shapes" style={{ color: '#fff', fontSize: '12px', textTransform: 'capitalize' }} />
          <Tab icon={<InsertPhotoTwoToneIcon />} label="Images" style={{ color: '#fff', fontSize: '12px', textTransform: 'capitalize' }} />
          <Tab icon={<TextFieldsIcon />} label="Texts" style={{ color: '#fff', fontSize: '12px', textTransform: 'capitalize' }} />

        </Tabs>
      </Box>
        
      </div>
      <div className="tab-panel">
        {/* <div className="searching">
          <Search style={{ backgroundColor: "#fff" }}>
            <SearchIconWrapper>
              <SearchIcon style={{ color: "#000" }} />
            </SearchIconWrapper>
            <StyledInputBase
              style={{ color: "#000" }}
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
        </div> */}
        <div className="all-shapes">
          <TabPanel className="aaab" value={value} index={0} style={{ width: "-webkit-fill-available" }}>
            <div item xs={12} style={{ backgroundColor: "#3d3d3d", height: window.innerHeight }}>
              <div item xs={11.8} style={{ backgroundColor: "transparent", padding: '2px' }}>
                <AllShapes style={{ display: "none" }} shapeList={props.shapeList} dragUrl={props.dragUrl} />
              </div>
            </div>
          </TabPanel>
          <TabPanel value={value} index={1} style={{ width: "-webkit-fill-available" }}>
            <div item xs={12} style={{ backgroundColor: "rgb(68 68 68)", height: window.innerHeight }}>
              <div item xs={11.8} style={{ backgroundColor: "transparent", padding: '2px' }}>
                <AllImages style={{ display: "none" }} pictureList={props.pictureList} dragUrl={props.dragUrl} />
              </div>
            </div>
          </TabPanel>
          <TabPanel value={value} index={2} style={{ width: "-webkit-fill-available" }}>
            <div item xs={12} style={{ backgroundColor: "rgb(68 68 68)", height: window.innerHeight }}>
              <div item xs={11.8} style={{ backgroundColor: "transparent", padding: '2px' }}>
                <AllText style={{ display: "none" }} textList={props.textList} dragUrl={props.dragUrl} />
              </div>
            </div>
          </TabPanel>
        </div>
      </div>
    </div>
  )
}

export default SideNavbar
