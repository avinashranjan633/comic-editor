import React from 'react'
// import {useState} from 'react'
import NavBar from './Navbar'
import AddIcon from '@mui/icons-material/Add';
import { Stage, Circle, Layer, Rect, Image, Transformer } from 'react-konva';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { useState, useEffect } from 'react';
import SideNavbar from './SideNavbarBackup';


import { useSelector, useDispatch } from "react-redux";
import useImage from 'use-image';
import { ShapeProperty, ImageProperty } from '../common/Constant';







const NewShape = ({ shapeProps, isSelected, onSelect, onChange, newColor, flag }) => {
  const shapeRef = React.useRef();
  const trRef = React.useRef();
  React.useEffect(() => {
    if (isSelected) {
      // we need to attach transformer manually
      trRef.current.nodes([shapeRef.current]);
      trRef.current.getLayer().batchDraw();
    }
  }, [isSelected]);
  const [img] = useImage(shapeProps.url);
  const [temp, settemp] = React.useState(flag)
  if (isSelected && temp == 1) {
    shapeProps.fill = newColor;
    settemp(0)
  }
  return (
    <React.Fragment>
      <shapeProps.type
        onClick={onSelect}
        onTap={onSelect}
        ref={shapeRef}
        image={img}
        {...shapeProps}
        draggable
        onDragEnd={(e) => {
          onChange({
            ...shapeProps,
            x: e.target.x(),
            y: e.target.y(),
          });
        }}

        onTransformEnd={(e) => {
          // transformer is changing scale of the node
          // and NOT its width or height
          // but in the store we have only width and height
          // to match the data better we will reset scale on transform end
          const node = shapeRef.current;
          const scaleX = node.scaleX();
          const scaleY = node.scaleY();
          // we will reset it back
          node.scaleX(1);
          node.scaleY(1);

          // console.log(shapeProps.innerRadius)
          shapeProps.innerRadius ? onChange({
            ...shapeProps,
            // keepRatio: true,
            x: node.x(),
            y: node.y(),
            // set minimal value
            width: Math.max(5, node.width() * scaleX),
            height: Math.max(node.height() * scaleY),
            innerRadius: Math.max(node.innerRadius() * scaleY),
          })
            : onChange({
              ...shapeProps,
              // keepRatio: true,
              x: node.x(),
              y: node.y(),
              // set minimal value
              width: Math.max(5, node.width() * scaleX),
              height: Math.max(node.height() * scaleY),
            })

        }}
      />
      {isSelected && (
        <Transformer
          ref={trRef}
          ignoreStroke={true}
          // enabledAnchors={['top-left', 'top-right','bottom-left', 'bottom-right']}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </React.Fragment>
  );
};






const MobileLayout = () => {

  const [showEditTools, setShowEditTools] = useState(false)






  const dragUrl = React.useRef();
  const stageRef = React.useRef();
  const [pictureList, setPictureList] = React.useState([]);
  const images = ImageProperty;
  const shapeList = ShapeProperty;
  // ===========================================
  const state = useSelector((state) => state.AppFilter);
  const [selectedId, selectShape] = React.useState(null);






  const openbtn = (e) => {
    if (showEditTools !== "mob-edit-tools") {
      setShowEditTools(true);
    }
  }

  useEffect(() => {
    // document.body.addEventListener("click", openbtn)
    // return () => {
    //     document.body.removeEventListener("click", openbtn)
    // }
  })


  return (
    <div className='mob-main-div'>
      <div className="mob-header">
        <NavBar />
      </div>

      <div className="mob-content-area">

        {/* <div className="mob-edit-area"> */}
        {/* Edit part */}

        <div
          onDrop={(e) => {
            e.preventDefault();
            console.log(dragUrl.current)
            let newElement = dragUrl.element_property
            setPictureList([...pictureList, newElement]);
          }}
          onDragOver={(e) => e.preventDefault()}
          className="edit-layer">
          <div item xs={12}>
            <Container maxWidth="sm">
              {/* <ImageDrag /> */}
              <div container xs={12} spacing={1}>
              </div>
              <div
                onDrop={(e) => {
                  e.preventDefault();
                  // add image
                  console.log(dragUrl.current)
                  let newElement = dragUrl.element_property
                  setPictureList([...pictureList, newElement]);
                }}
                onDragOver={(e) => e.preventDefault()}
              >
                <Stage width={550} height={450} ref={stageRef} style={{ border: '1px solid grey', marginTop: '30px', background: '#fff' }}>
                  <Layer >
                    {pictureList.map((newimage, i) => {
                      newimage.id = i
                      // dispatch(setFlagChange(0))
                      // newimage.fill = state.color
                      return (
                        <NewShape
                          onKeyDown={() => alert('key pressed')}
                          type={newimage.type}
                          key={i}
                          shapeProps={newimage}
                          isSelected={newimage.id === selectedId}
                          newColor={state.color}
                          flag={state.flag}

                          onSelect={() => {
                            selectShape(newimage.id);
                            // newimage.fill = state.color
                          }}
                          onChange={(newAttrs) => {
                            const newimages = pictureList.slice();
                            newimages[i] = newAttrs;
                            setPictureList(newimages);
                          }}

                        />
                      );
                    })}
                    {/* <DrawAllShapes InitialShape={shapeList} /> */}
                  </Layer>
                </Stage>
              </div>
            </Container>
          </div>
        </div>

        {/* </div> */}
      </div>

      <div className="mobile-footer">
        <button className="mob-edit-btn"
          // onClick={() => setShowEditTools(prev => !prev)}
          onClick={openbtn}
        >
          <AddIcon />
        </button>
        <button className="mob-page-count-btn">
          <div className="mob-count-no">
            <div>1</div>
          </div>
        </button>

        {showEditTools &&
          <div className="mob-edit-tools">
            <div className="mob-closed-dash"></div>
            <div className="mob-edit-tools-sec">
              <SideNavbar pictureList={images} shapeList={shapeList} dragUrl={dragUrl} />
            </div>
          </div>
        }
      </div>

    </div>
  )
}

export default MobileLayout