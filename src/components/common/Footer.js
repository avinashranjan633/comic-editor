import React, { Component } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

export default function Footer() {
  return (
    <Box sx={{ flexGrow: 1 }} className="footer-content">
      <AppBar position="static" style={{ background: "#fff", color: "#000" }}>

        <Toolbar>
          <IconButton
            className="footer-open-arrow"
          >
            <ArrowDropUpIcon />
          </IconButton>

          Hello This is footer
        </Toolbar>
      </AppBar>
    </Box>
  )
}

