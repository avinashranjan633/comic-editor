import React, { Component } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button'; 
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import NavBar from './Navbar';
import ColorPicker from '../filter/ColorPicker';
import BorderColor from '../filter/BorderColor';
import Border from '../filter/Border';
import Opacity from '../filter/Opacity';
export default function NavToolBar() {
    return (
        <Box sx={{ flexGrow: 2 }} className="raised-button-rounded">
            <AppBar position="static" color="secondary" style={{height:'72px'}}>

                <Toolbar>
                    {/* <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        sx={{ mr: 2 }} >
                        <MenuIcon />
                    </IconButton> */}
                    <div className='row'>
                        <div className='col' style={{ 'textAlign': 'center' }}>
                            Solid
                            <div style={{ "display": "flex" }}>
                                <ColorPicker />
                            </div>
                        </div >
                        <div className='col' style={{ 'textAlign': 'center' }}>
                            Border
                            <div style={{ "display": "flex" }}>
                                <Border />
                                <BorderColor />
                            </div>
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

