import React from 'react'
import SideNavbar from './SideNavbar'
import { useSelector, useDispatch } from "react-redux";
import { Stage, Circle, Layer, Rect, Image, Transformer } from 'react-konva';
import { ShapeProperty, ImageProperty , TextProperty} from '../common/Constant';
import useImage from 'use-image';
import NavBar from './Navbar';
import Footer from './Footer';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import NavToolBar from './NavToolBar';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { setChangeColor, setFlagChange } from "../filter/Filter"
import { display } from '@mui/system';


const NewShape = ({ shapeProps, isSelected, onSelect, onChange, state }) => {
  const shapeRef = React.useRef();
  const trRef = React.useRef();
  React.useEffect(() => {
    if (isSelected) {
      // we need to attach transformer manually
      trRef.current.nodes([shapeRef.current]);
      trRef.current.getLayer().batchDraw();
    }
  }, [isSelected]);

  const [img] = useImage(shapeProps.url);
  if (isSelected) {
    if (state.flag == 1) {
      shapeProps.fill = state.color
    } else if (state.flag == 2) {
      shapeProps.stroke = state.bordercolor
    }
    else if (state.flag == 3) {
      shapeProps.strokeWidth = state.bordersize
    }
  }
  return (
    <React.Fragment>
      <shapeProps.type
        onClick={onSelect}
        onTap={onSelect}
        ref={shapeRef}
        image={img}
        {...shapeProps}
        draggable
        onDragEnd={(e) => {
          onChange({
            ...shapeProps,
            x: e.target.x(),
            y: e.target.y(),
          });
        }}

        onTransformEnd={(e) => {
          // transformer is changing scale of the node
          // and NOT its width or height
          // but in the store we have only width and height
          // to match the data better we will reset scale on transform end
          const node = shapeRef.current;
          const scaleX = node.scaleX();
          const scaleY = node.scaleY();
          // we will reset it back
          // console.log(node.attrs.type)
          if(node.attrs.type == 'Text'){
            return 0
          }else{
            node.scaleX(1);
            node.scaleY(1);
          }

          // console.log(shapeProps.innerRadius)
          // alert(Math.max(node.height() * scaleY))
          shapeProps.innerRadius ? onChange({
            ...shapeProps,
            keepRatio: true,
            x: node.x(),
            y: node.y(),
            // set minimal value
            width: Math.max(5, node.width() * scaleX),
            height: Math.max(node.height() * scaleY),
            innerRadius: Math.max(node.innerRadius() * scaleY),
          })
            : onChange({
              ...shapeProps,
              // keepRatio: true,
              x: node.x(),
              y: node.y(),
              // set minimal value
              width: Math.max(5, node.width() * scaleX),
              height: Math.max(node.height() * scaleY),
            })

        }}
      />
      {isSelected && (
        <Transformer
          ref={trRef}
          ignoreStroke={true}
          keepRatio={true}
          centeredScaling={true}
          // enabledAnchors={['top-left', 'top-right','bottom-left', 'bottom-right']}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </React.Fragment>
  );
};



const Layout = () => {

  const dragUrl = React.useRef();
  const stageRef = React.useRef();
  const [pictureList, setPictureList] = React.useState([]);
  const images = ImageProperty;
  const shapeList = ShapeProperty;
  const textList = TextProperty;
  // ===========================================
  const state = useSelector((state) => state.AppFilter);
  const [selectedId, selectShape] = React.useState(null);
  const dispatch = useDispatch();


  return (
    <div className="main-layout">
      <NavBar />
      <div className="layout" style={{ display: 'flex' }}>
        <div className="col-md-3 sidebar">
          <SideNavbar pictureList={images} shapeList={shapeList} textList={textList} dragUrl={dragUrl} />
          {/* <button className='sidebar-closed-btn'><ArrowBackIosIcon className='closed-btn ' /></button> */}
        </div>
        <div className=" col-md-9 main-layer">
          <NavToolBar />
          <div
            onDrop={(e) => {
              e.preventDefault();
              console.log(dragUrl.current)
              let newElement = dragUrl.element_property
              setPictureList([...pictureList, newElement]);
            }}
            onDragOver={(e) => e.preventDefault()}
            className="edit-layer">
            <div item xs={12}>
              <Container maxWidth="sm">
                {/* <ImageDrag /> */}
                <div className="container" spacing={1}>
                </div>
                <div
                  onDrop={(e) => {
                    e.preventDefault();
                    // add image
                    console.log(dragUrl.current)
                    let newElement = dragUrl.element_property
                    setPictureList([...pictureList, newElement]);
                  }}
                  onDragOver={(e) => e.preventDefault()}
                >
                  <Stage width={478} height={500} ref={stageRef} style={{ border: '1px solid grey', marginTop: '30px', background: '#fff',display: '-webkit-inline-box' }}>
                    <Layer >
                      {pictureList.map((newimage, i) => {
                        newimage.id = i

                        return (
                          <NewShape
                            onKeyDown={() => alert('key pressed')}
                            type={newimage.type}
                            key={i}
                            shapeProps={newimage}
                            isSelected={newimage.id === selectedId}
                            // newColor={state.color}
                            // flag={state.flag}
                            state={state}
                            onSelect={() => {
                              alert("clicked")
                              selectShape(newimage.id);
                              dispatch(setFlagChange(0))
                            }}
                            onClick={()=>{
                              alert("clicked")
                            }}
                            onChange={(newAttrs) => {
                              const newimages = pictureList.slice();
                              newimages[i] = newAttrs;
                              setPictureList(newimages);
                            }}

                          />
                        );
                      })}
                      {/* <DrawAllShapes InitialShape={shapeList} /> */}
                    </Layer>
                  </Stage>
                </div>
              </Container>
            </div>
          </div>
          {/* <div className="footer"><Footer /></div> */}
        </div>
      </div>
    </div>
  )
}

export default Layout