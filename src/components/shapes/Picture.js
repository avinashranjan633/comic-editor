import React, { Component } from 'react';
import Grid from '@mui/material/Grid';

const Picture = (props) => {
  
  return (
    <div style={{padding:'5px'}} >
      <img 
        id={props.id}
        width={100}
        height={100}
        src={props.url}
        onDragStart={props.onDragStart}

      />
    </div>
  );
};


export default Picture;