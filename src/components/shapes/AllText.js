import React from 'react';
import Grid from '@mui/material/Grid';
import Picture from './Picture';
import TextView from './TextView';

const AllText = (props) => {
  return (
    <>
      <div className="shaps">

        {props.textList.map((propText, i) => {
          return (
            <div className="shaps-layout" style={{ display: 'inline-block' }}>
              <TextView
                id={i}
                url={propText.url}
                onDragStart={(e) => {
                  props.dragUrl.current = e.target.src;
                  props.dragUrl.element_property = propText;
                }}
              />
            </div>
          )
        })}
      </div>
    </>
  )
}
export default AllText