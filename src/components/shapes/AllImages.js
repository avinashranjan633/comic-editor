import React from 'react';
import Grid from '@mui/material/Grid';
import Picture from './Picture';

const AllImages = (props) => {

  return (
    <>
      <div className="shaps">

        {props.pictureList.map((propImage, i) => {
          return (
            <div className="shaps-layout" style={{ display: 'inline-block' }}>
              <Picture
                id={i}
                url={propImage.url}
                onDragStart={(e) => {
                  props.dragUrl.current = e.target.src;
                  props.dragUrl.element_property = propImage;
                }}
              />
            </div>
          )
        })}
      </div>
    </>
  )
}
export default AllImages