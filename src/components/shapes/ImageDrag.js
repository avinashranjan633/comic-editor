import React from 'react';
import { createRoot } from 'react-dom/client';
import { Stage, Layer, Image } from 'react-konva';
import useImage from 'use-image';
import AllImages from './AllImages';
import Grid from '@mui/material/Grid';
import Picture from './Picture';

const URLImage = ({ image }) => {
  const [img] = useImage(image.src);
  return (
    <Image
      draggable
      image={img}
      x={image.x}
      y={image.y}
      // I will use offset to set origin to the center of the image
      offsetX={img ? img.width / 2 : 0}
      offsetY={img ? img.height / 2 : 0}
    />
  );
};

const ImageDrag = () => {
  const dragUrl = React.useRef();
  const stageRef = React.useRef();
  const [images, setImages] = React.useState([]);
  const pictureList = [
    {
      id: 1,
      url: 'https://konvajs.org/assets/lion.png'
    },
    {
      id: 2,
      url: 'https://konvajs.org/assets/yoda.jpg'
    },

  ]
  return (
    <div>
      Try to trag and image into the stage:
      <br />
      <div container xs={12} spacing={1}>

        {pictureList.map((picture) => {
          return <Picture
            id={picture.id}
            url={picture.url}
            draggable="true"
            onDragStart={(e) => {
              dragUrl.current = e.target.src;
            }}
          />
        })}
      </div>
      {/* <img
        alt="lion"
        src="https://konvajs.org/assets/lion.png"
        draggable="true"
        onDragStart={(e) => {
          dragUrl.current = e.target.src;
        }}
      />
      <img
        alt="yoda"
        src="https://konvajs.org/assets/yoda.jpg"
        draggable="true"
        onDragStart={(e) => {
          dragUrl.current = e.target.src;
        }}
      /> */}
      {/* <AllImages dragurl={dragUrl}/> */}
      <div
        onDrop={(e) => {
          e.preventDefault();
          // register event position
          stageRef.current.setPointersPositions(e);
          // add image
          setImages(
            images.concat([
              {
                ...stageRef.current.getPointerPosition(),
                src: dragUrl.current,
              },
            ])
          );
        }}
        onDragOver={(e) => e.preventDefault()}
      >
        <Stage
          width={window.innerWidth}
          height={window.innerHeight}
          style={{ border: '1px solid grey' }}
          ref={stageRef}
        >
          <Layer>
            {images.map((image) => {
              return <URLImage image={image} />;
            })}
          </Layer>
        </Stage>
      </div>
    </div>
  );
};

export default ImageDrag;

