import React from 'react';
import Grid from '@mui/material/Grid';
import Shape from './Shape';

const AllShapes = (props) => {
  const handleClick = event => {
    console.log(event.detail);
    switch (event.detail) {
      case 1: {
        console.log('single click');
        break;
      }
      case 2: {
        console.log('double click');
        break;
      }
      case 3: {
        console.log('triple click');
        break;
      }
      default: {
        break;
      }
    }
  };
  return (
    <>
      <div className="shaps">

        {props.shapeList.map((propshape, i) => {
          // return <URLImage image={image} />;
          return (
          <div className="shaps-layout" style={{display: 'inline-block'}}>
            <Shape
              property={propshape}
              id={i}
              url={propshape.url}
              onClick={handleClick}
              onDragStart={(e) => {
                console.log(propshape)
                props.dragUrl.current = e.target.src;
                props.dragUrl.element_property = propshape;

              }}
            />

          </div>
          )
        })}
      </div>
      {/* <div container spacing={0.5}>
        {props.shapeList.map((propshape, i) => {
          // return <URLImage image={image} />;
          return <Shape
            property={propshape}
            id={i}
            url={propshape.url}
            onDragStart={(e) => {
              console.log(propshape)
              props.dragUrl.current = e.target.src;
              props.dragUrl.element_property = propshape;

            }}
          />
        })}
      </div> */}
    </>
  )
}
export default AllShapes