import React, { Component } from 'react';
import Grid from '@mui/material/Grid';
import { Stage, Circle, Layer, Rect, Transformer } from 'react-konva';

const Shape = (props) => {
  return (
    <div style={{padding: '5px'}} >
      <img
        // {...props.property}
        id={props.key}
        width={100}
        height={100}
        src={props.url}
        onDragStart={props.onDragStart}
        // style={{ width: '100px', height:'100px' }}
      />
    </div>
  );
};


export default Shape;