import React, { Component } from 'react';
// import React from 'react';
import { createRoot } from 'react-dom/client';
import { Stage,Circle, Layer, Rect, Transformer } from 'react-konva';
import useImage from 'use-image';
import Grid from '@mui/material/Grid';


const NewShape = ({ shapeProps, isSelected, onSelect, onChange }) => {
    const shapeRef = React.useRef();
    const trRef = React.useRef();
    
    React.useEffect(() => {
        if (isSelected) {
            // we need to attach transformer manually
            trRef.current.nodes([shapeRef.current]);
            trRef.current.getLayer().batchDraw();
        }
    }, [isSelected]);
    const [img] = useImage(shapeProps.url);
    return (
        <React.Fragment>
            <shapeProps.type
                onClick={onSelect}
                onTap={onSelect}
                ref={shapeRef}
                image={img}
                {...shapeProps}
                draggable
            
                onDragEnd={(e) => {
                    console.log("dwhodiwhdowh")

                    onChange({
                        ...shapeProps,
                        x: e.target.x(),
                        y: e.target.y(),
                    });
                }}
                onTransformEnd={(e) => {
                    // transformer is changing scale of the node
                    // and NOT its width or height
                    // but in the store we have only width and height
                    // to match the data better we will reset scale on transform end
                    const node = shapeRef.current;
                    const scaleX = node.scaleX();
                    const scaleY = node.scaleY();

                    // we will reset it back
                    node.scaleX(1);
                    node.scaleY(1);

                    onChange({
                        ...shapeProps,
                        // keepRatio: true, 
                        x: node.x(),
                        y: node.y(),
                        // set minimal value
                        width: Math.max(5, node.width() * scaleX),
                        height: Math.max(node.height() * scaleY),
                        fontSize:Math.max(node.height() * scaleY),
                    });
                }}
            />
            {isSelected && (
                <Transformer
                    ref={trRef}
                    ignoreStroke={true}
                    // enabledAnchors={['top-left', 'top-right','bottom-left', 'bottom-right']}
                    boundBoxFunc={(oldBox, newBox) => {
                        // limit resize
                        if (newBox.width < 5 || newBox.height < 5) {
                            return oldBox;
                        }
                        return newBox;
                    }}
                />
            )}
        </React.Fragment>
    );
};
// var imageObj = new Image();
// imageObj.src = '"../../assets/images/user.jpg"';


const DrawAllShapes = (props) => {

    // const InitialShape= props.InitialShape
    const [shapes, setShapes] = React.useState(props.InitialShape);
    const [selectedId, selectShape] = React.useState(null);

    const checkDeselect = (e) => {
        // deselect when clicked on empty area
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
            selectShape(null);
        }
    };

    return (

        <>
        <div container xs={12} spacing={1}>
            {shapes.map((newShape, i) => {
                return (
                    <NewShape
                        type={newShape.type}
                        key={i}
                        shapeProps={newShape}
                        isSelected={newShape.id === selectedId}
                        onSelect={() => {
                            selectShape(newShape.id);
                        }}
                        onChange={(newAttrs) => {
                            const newshapes = shapes.slice();
                            newshapes[i] = newAttrs;
                            setShapes(newshapes);
                        }}
                    />
                );
            })}
        </div>
        </>

    );
};




export {NewShape};
export default DrawAllShapes;