import React, { Component } from 'react';
import { Circle } from 'react-konva';

export default class Circles extends Component {
  render() {
    return (
      <Circle draggable x={200} y={100} radius={50} fill="green" />

    );
  }
}
