import React from 'react'
import { SketchPicker } from 'react-color'
import reactCSS from 'reactcss'
import ColorLensIcon from '@mui/icons-material/ColorLens';
import { useSelector, useDispatch } from "react-redux";
import {setChangeBorderColor, setFlagChange} from "./Filter"

export default function BorderColor() {

  const [showPicker,setshowPicker] = React.useState(false)
  const [colorHex, setColorHex] = React.useState('#FFFFFF')
  const dispatch = useDispatch();

  const onClick = () => {
    setshowPicker(!showPicker)
  };

  const onClose = () => {
    setshowPicker(false)
  };

  const onChange = (color) => {
    setColorHex(color.hex)
    dispatch(setChangeBorderColor(color.hex));
    dispatch(setFlagChange(2))
  };



  const styles = reactCSS({
    'default': {
      color: {
        width: '15px',
        height: '15px',
        borderRadius: '3px',
        // background: `rgba(${this.state.color.r}, ${this.state.color.g}, ${this.state.color.b}, ${this.state.color.a})`,
        background: colorHex,
        
      },
      popover: {
        position: 'absolute',
        zIndex: '3',
      },
      cover: {
        position: 'fixed',
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px',
      },

    },
  });

  return (
    <div>

    <div style={{ 'display': 'flex' }}>

      <ColorLensIcon  onClick={onClick}/>
      <div style={styles.color} />
    </div>

    {showPicker ? <div style={styles.popover}>
      <div style={styles.cover} onClick={onClose} />
      <SketchPicker color={colorHex} onChange={onChange} />
    </div> : null}

  </div>
  )

}

