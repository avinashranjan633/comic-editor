export default function DraggableProgress() {
    var lastX;
    var rect = document.querySelector('.progressdiv');
    
    const fun1=(event) =>{

        if (event.which === 1) {
            lastX = event.pageX;
            moved()
            // addEventListener('mousemove', moved);
            event.preventDefault(); // Prevent selection
        }
    }

    function buttonPressed(event) {
        // not all browsers support event.which for mousemove, but
        // all major browsers support buttons or which
        if (event.buttons == null)
            return event.which != 0;
        else
            return event.buttons != 0;
    }
    function moved(event) {
        if (!buttonPressed(event)) {
            moved()
            // removeEventListener('mousemove', moved);
        } else {
            var dist = event.pageX - lastX;
            var newWidth = Math.max(10, rect.offsetWidth + dist);
            rect.style.width = newWidth + 'px';
            lastX = event.pageX;
        }
    }

    return(
        <div className="progressdiv" onMouseDown={moved} style={{ "background": "orange", "width": "60px", "height": "20px"}}> </div>
    )
}