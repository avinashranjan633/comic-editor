import { createSlice } from '@reduxjs/toolkit'

export const Filter = createSlice({
  name: 'filter',
  initialState: {
    value: 0,
    color: '#FFFFFF',
    bordercolor:'#FFFFFF',
    bordersize:1,
    flag: 0,
  },
  reducers: {
    setChangeColor: (state, action) => {
      state.color = action.payload
    },
    setChangeBorderColor: (state, action) => {
      state.bordercolor = action.payload
    },
    setFlagChange: (state, action) => {
      state.flag = action.payload
    },
    setChangeBorder: (state, action) => {
      state.bordersize = action.payload
    },
  },
})

export const {setChangeBorderColor, setChangeColor,setFlagChange, setChangeBorder } = Filter.actions

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched

// export const incrementAsync = (amount) => (dispatch) => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount))
//   }, 1000)
// }

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
// export const selectFilter = (state) => state.Filter.color

export default Filter.reducer
