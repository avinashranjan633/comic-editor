import React from 'react'
import { useSelector, useDispatch } from "react-redux";
import {setFlagChange, setChangeBorder} from "./Filter"
import DraggableProgress from './DraggableProgress';
export default function Opacity() {

  const dispatch = useDispatch();

  const onChange = (val) => {
    console.log(val)
    dispatch(setChangeBorder(val));
    dispatch(setFlagChange(3))
    
  };

  return (
    <div>

      <div style={{ 'display': 'flex' }}>

      <DraggableProgress />
      </div>
    </div>
  )

}

